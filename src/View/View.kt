package src.View

interface View
{
    fun show()
}