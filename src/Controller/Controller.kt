package src.Controller

import src.View.View

interface Controller {

    fun onCreate()
    fun update(view: View?,action:ActionController?)
    fun show()
    fun onExit()
}