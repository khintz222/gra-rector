package src.Controller

interface ActionController {
    fun run()
}