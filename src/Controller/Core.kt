package src.Controller

import src.View.*

class Core : Controller{
    var activeView: View
    init {
        view = Menu()
    }
    var isGameRun = true
    override fun onCreate() {
        var view = Menu()

        do {
            var goodAnswer = false
            var answer = readLine()!!.toInt()
            when(answer)
            {
                1->update(Game(),null)
                2->update(Status(),null)
                3->update(Exit(),null)
            }
        }while (goodAnswer)

    }


    override fun update(view: View?, action: ActionController?) {
        if(view != null )
        {
            activeView = view
        }else{
            action.run()
        }
    }

    override fun show() {
        view.show()
    }

    override fun onExit() {
        TODO("not implemented") //To change body of created functions use File | Settings | File Templates.
    }
}